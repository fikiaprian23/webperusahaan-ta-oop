import 'package:chatbro_admin/constans.dart';
import 'package:chatbro_admin/environment.dart';
import 'package:chatbro_admin/root_app.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "AIzaSyDD6zwTOxSRuKCQjNDHwC_KRdRWVEdhA9U",
      projectId: "chatbro-backend",
      messagingSenderId: "520165631723",
      appId: "1:520165631723:web:bba342f095dc88debf40ae",
      storageBucket: "myapp.appspot.com",
    ),
  );
  // FirebaseAuth auth = FirebaseAuth.instance;
  // await auth.signOut();
  print(firebaseAuth.currentUser);

  Environment.init(
    apiBaseUrl: 'https://chatbro.com',
  );

  runApp(
    const ProviderScope(
      child: RootApp(),
    ),
  );
}
