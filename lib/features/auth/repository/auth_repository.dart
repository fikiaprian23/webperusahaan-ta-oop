import 'dart:convert';
import 'dart:io';
import 'package:chatbro_admin/config/api_response.dart';
import 'package:http/http.dart' as http;
import 'package:chatbro_admin/app_router.dart';
import 'package:chatbro_admin/common/repositories/common_firebase_storage_repository.dart';
import 'package:chatbro_admin/common/widgets/show_alert_dialog.dart';
import 'package:chatbro_admin/constans.dart';

import 'package:chatbro_admin/models/admin_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

final authRepositoryProvider = Provider(
  (ref) => AuthRepository(
    auth: firebaseAuth,
    firestore: firestore,
  ),
);

class AuthRepository {
  final FirebaseAuth auth;
  final FirebaseFirestore firestore;
  AuthRepository({
    required this.auth,
    required this.firestore,
  });

  Future<AdminModel?> getCurrentUserData() async {
    var adminData =
        await firestore.collection('admins').doc(auth.currentUser?.uid).get();

    AdminModel? admin;
    if (adminData.data() != null) {
      admin = AdminModel.fromMap(adminData.data()!);
    }
    return admin;
  }

  void signupWithEmail(BuildContext context, String username, String email,
      String password, WidgetRef ref) async {
    try {
      final credential = await auth.createUserWithEmailAndPassword(
          email: email, password: password);

      print(credential);
      // kirim email verifikasi
      // await credential.user!.sendEmailVerification();

      // simpan  ke firestore
      // ignore: use_build_context_synchronously
      saveUserDataToFirebase(
          username: username, profilePic: null, ref: ref, context: context);
    } on FirebaseAuthException catch (e) {
      print(e.toString());
    }
  }

  void saveUserDataToFirebase({
    required String username,
    required File? profilePic,
    required WidgetRef ref,
    required BuildContext context,
  }) async {
    try {
      String uid = auth.currentUser!.uid;
      String photoUrl = 'https://picsum.photos/200/300';

      if (profilePic != null) {
        photoUrl = await ref
            .read(commonFirebaseStorageRepositoryProvider)
            .storeFileToFirebase(
              'profilePic/$uid',
              profilePic,
            );
      }

      var adminData = {
        'username': username,
        'uid': uid,
        'profilePic': photoUrl,
        'email': auth.currentUser!.email!,
      };

      // Mengirim data admin ke API untuk disimpan
      var response = await saveAdminToAPI(adminData);

      if (response.statusCode == 200) {
        // ignore: use_build_context_synchronously
        showAlertDialog(context: context, message: response.data);
        // print(response.body);
      } else {
        // ignore: use_build_context_synchronously
        showAlertDialog(
            context: context,
            message: 'Gagal menyimpan data admin: ${response.data}');
        // print('Gagal menyimpan data admin: ${response.body}');
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<ApiResponse> saveAdminToAPI(Map<String, dynamic> adminData) async {
    var url = 'http://localhost:8080/api/admins';

    try {
      var response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode(adminData),
      );

      return ApiResponse(
        statusCode: response.statusCode,
        data: response.body,
      );
    } catch (e) {
      return ApiResponse(
        statusCode: -1, // Contoh kode status khusus untuk kesalahan jaringan
        data: 'Terjadi kesalahan jaringan: ${e.toString()}',
      );
    }
  }

  Future<void> login(
      BuildContext context, String email, String password) async {
    try {
      final credential = await auth.signInWithEmailAndPassword(
          email: email, password: password);
      print(credential);
      if (credential.user != null) {
        // ignore: use_build_context_synchronously
        GoRouter.of(context).go(RouteUri.home);
      }
    } on FirebaseAuthException catch (e) {
      showAlertDialog(context: context, message: e.toString());
      print(e.toString());
    }
  }

  void logout() async {
    try {
      await auth.signOut();
    } catch (e) {
      print(e);
    }
  }
}
