import 'package:chatbro_admin/features/auth/repository/auth_repository.dart';
import 'package:chatbro_admin/models/admin_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final authControllerProvider = Provider((ref) {
  final authRepository = ref.watch(authRepositoryProvider);
  return AuthController(authRepository: authRepository, ref: ref);
});

final adminDataAuthProvider = FutureProvider((ref) {
  final authController = ref.watch(authControllerProvider);
  return authController.getAdminData();
});

class AuthController {
  final AuthRepository authRepository;
  final ProviderRef ref;
  AuthController({
    required this.authRepository,
    required this.ref,
  });

  Future<AdminModel?> getAdminData() async {
    AdminModel? Admin = await authRepository.getCurrentUserData();
    return Admin;
  }

  void signupWithEmail(BuildContext context, String username, String email,
      String password, WidgetRef ref) {
    authRepository.signupWithEmail(context, username, email, password, ref);
  }

  Future<void> login(
      BuildContext context, String email, String password) async {
    await authRepository.login(context, email, password);
  }
}
