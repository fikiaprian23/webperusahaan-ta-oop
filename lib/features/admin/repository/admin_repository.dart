import 'dart:convert';
import 'dart:io';

import 'package:chatbro_admin/common/repositories/common_firebase_storage_repository.dart';
import 'package:chatbro_admin/common/utils/util.dart';
import 'package:chatbro_admin/common/widgets/show_alert_dialog.dart';
import 'package:chatbro_admin/constans.dart';
import 'package:chatbro_admin/models/admin_model.dart';
import 'package:chatbro_admin/models/status_model.dart';
import 'package:chatbro_admin/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;

final adminRepositoryProvider = Provider(
  (ref) => AdminRepository(
    auth: firebaseAuth,
    firestore: firestore,
    firebaseStorage: firebaseStorage,
    ref: ref,
  ),
);

class AdminRepository {
  final FirebaseAuth auth;
  final FirebaseFirestore firestore;
  final FirebaseStorage firebaseStorage;
  final ProviderRef ref;
  AdminRepository({
    required this.auth,
    required this.firestore,
    required this.firebaseStorage,
    required this.ref,
  });

  Future<AdminModel?> getCurrentAdminData() async {
    try {
      var uid = auth.currentUser?.uid;
      var apiUrl = 'http://localhost:8080/api/admins/$uid';

      var response = await http.get(Uri.parse(apiUrl));

      if (response.statusCode == 200) {
        var responseData = jsonDecode(response.body);
        if (responseData != null) {
          return AdminModel.fromMap(responseData['data']);
        }
      }
    } catch (e) {
      print('Error occurred while fetching user data: $e');
    }

    return null;
  }

  Future<List<AdminModel>> getAllAdmins() async {
    var url = 'http://localhost:8080/api/admins'; // Ganti dengan URL API Anda

    try {
      var response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        var responseData = json.decode(response.body);
        List<AdminModel> admins = [];

        if (responseData['status'] == 'OK') {
          var adminDataList = responseData['data'];

          for (var adminData in adminDataList) {
            AdminModel admin = AdminModel.fromMap(adminData);

            admins.add(admin);
          }
        }

        return admins;
      }
    } catch (e) {
      print('Error occurred while fetching admins data: $e');
    }

    return [];
  }

  Future<void> updateProfile({
    required BuildContext context,
    required WidgetRef ref,
    String? username,
    Uint8List? profilePic,
  }) async {
    try {
      final user = auth.currentUser;
      if (user != null) {
        if (username != null) {
          await firestore
              .collection('admins')
              .doc(user.uid)
              .update({'username': username});
        }

        if (profilePic != null) {
          // Upload profile picture to Firebase Storage
          final storageRepository =
              ref.read(commonFirebaseStorageRepositoryProvider);
          final downloadUrl = await storageRepository.storeUint8ListToFirebase(
            'profilePic/${user.uid}',
            profilePic,
          );

          // Update profile picture URL in Firestore
          await firestore
              .collection('admins')
              .doc(user.uid)
              .update({'profilePic': downloadUrl});
        }
      }
    } catch (e) {
      // Handle any errors that occur during the update process
      showAlertDialog(context: context, message: e.toString());
    }
  }

  Future<List<UserModel>> getAllUsers() async {
    var url = 'http://localhost:8080/api/users'; // Ganti dengan URL API Anda

    try {
      var response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        var responseData = json.decode(response.body);
        List<UserModel> users = [];

        if (responseData['status'] == 'OK') {
          var userDataList = responseData['data'];

          for (var userData in userDataList) {
            UserModel user = UserModel.fromMap(userData);

            users.add(user);
          }
        }

        return users;
      }
    } catch (e) {
      print('Error occurred while fetching admins data: $e');
    }

    return [];
  }

  // Metode pencarian berdasarkan semua kolom
  Future<List<UserModel>> searchUsersByAllColumns(String query) async {
    var snapshot = await firestore.collection('users').get();

    List<UserModel> users = [];
    for (var doc in snapshot.docs) {
      UserModel user = UserModel.fromMap(doc.data());

      // Lakukan pencarian berdasarkan nama (tanpa memperhatikan huruf besar atau kecil) dan nomor telepon
      if (user.name.toLowerCase().contains(query.toLowerCase()) ||
          user.phoneNumber.toLowerCase().contains(query.toLowerCase())) {
        users.add(user);
      }
    }
    return users;
  }

  // Metode pencarian berdasarkan semua kolom
  Future<List<AdminModel>> searchAdminByAllColumns(String query) async {
    var snapshot = await firestore.collection('admins').get();

    List<AdminModel> users = [];
    for (var doc in snapshot.docs) {
      AdminModel user = AdminModel.fromMap(doc.data());

      // Lakukan pencarian berdasarkan nama (tanpa memperhatikan huruf besar atau kecil) dan nomor telepon
      if (user.username.toLowerCase().contains(query.toLowerCase()) ||
          user.email.toLowerCase().contains(query.toLowerCase())) {
        users.add(user);
      }
    }
    return users;
  }

  Future<void> uploadStatus({
    String? caption,
    File? statusImage,
    File? statusVideo,
    required BuildContext context,
  }) async {
    try {
      var statusId = const Uuid().v1();
      String uid = auth.currentUser!.uid;
      String fileUrl;

      if (statusImage != null) {
        fileUrl = await ref
            .read(commonFirebaseStorageRepositoryProvider)
            .storeFileToFirebase(
              '/status/$statusId$uid',
              statusImage,
            );
      } else {
        fileUrl = "";
      }

      caption ??= "";

      var usersSnapshot = await firestore.collection('users').get();
      List<String> uidWhoCanSee = [];
      for (var doc in usersSnapshot.docs) {
        uidWhoCanSee.add(doc.id);
      }

      List<String> statusFileUrls = [];
      var statusesSnapshot = await firestore
          .collection('status')
          .where(
            'uid',
            isEqualTo: auth.currentUser!.uid,
          )
          .get();

      if (statusesSnapshot.docs.isNotEmpty) {
        Status status = Status.fromMap(statusesSnapshot.docs[0].data());
        statusFileUrls = status.photoUrl;
        statusFileUrls.add(fileUrl);
        await firestore
            .collection('status')
            .doc(statusesSnapshot.docs[0].id)
            .update({
          'photoUrl': statusFileUrls,
          'captions': FieldValue.arrayUnion([caption]),
        });
        return;
      } else {
        statusFileUrls = [fileUrl];
      }

      Status status = Status(
        uid: uid,
        username: "WhatsApp",
        phoneNumber: "WhatsApp",
        photoUrl: statusFileUrls,
        createdAt: DateTime.now(),
        profilePic:
            'https://firebasestorage.googleapis.com/v0/b/chatbro-backend.appspot.com/o/profilePic%2Fwa.png?alt=media&token=f4161103-aa7a-4344-9bd4-ad3087c5fafa',
        statusId: statusId,
        whoCanSee: uidWhoCanSee,
        captions: [caption],
      );

      // ignore: use_build_context_synchronously
      await postStatusToFirestore(status, context);
    } catch (e) {
      showSnackBar(context: context, content: e.toString());
    }
  }

  Future<void> postStatusToFirestore(
      Status status, BuildContext context) async {
    try {
      var url = Uri.parse('http://localhost:8080/api/statuss');
      var headers = {'Content-Type': 'application/json'};
      var body = jsonEncode(status.toMap());

      var response = await http.post(url, headers: headers, body: body);

      if (response.statusCode == 200) {
        // ignore: use_build_context_synchronously
        showAlertDialog(
            context: context,
            message: 'Status posted to Firestore API successfully');
      } else {
        // ignore: use_build_context_synchronously
        showAlertDialog(
            context: context,
            message: 'Failed to post status to Firestore API');
      }
    } catch (e) {
      showAlertDialog(
          context: context,
          message: 'Error occurred while posting status to Firestore API: $e');
    }
  }
}
