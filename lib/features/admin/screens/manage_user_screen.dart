import 'dart:math';

import 'package:chatbro_admin/app_router.dart';
import 'package:chatbro_admin/common/widgets/card_elements.dart';
import 'package:chatbro_admin/common/widgets/portal_master_layout/portal_master_layout.dart';
import 'package:chatbro_admin/features/admin/controller/admin_controller.dart';
import 'package:chatbro_admin/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:chatbro_admin/common/constants/dimens.dart';
import 'package:chatbro_admin/common/generated/l10n.dart';
import 'package:chatbro_admin/common/theme/theme_extensions/app_button_theme.dart';
import 'package:chatbro_admin/common/theme/theme_extensions/app_data_table_theme.dart';

class ManageUsersScreen extends ConsumerStatefulWidget {
  const ManageUsersScreen({Key? key}) : super(key: key);

  @override
  ConsumerState<ManageUsersScreen> createState() => _ManageUsersScreenState();
}

class _ManageUsersScreenState extends ConsumerState<ManageUsersScreen> {
  final _scrollController = ScrollController();
  final _formKey = GlobalKey<FormBuilderState>();
  late Future<List<UserModel>> _usersFuture;
  int userCount = 0;

  @override
  void initState() {
    super.initState();
    _usersFuture = loadAllUser();
  }

  @override
  void dispose() {
    _scrollController.dispose();

    super.dispose();
  }

  Future<List<UserModel>> loadAllUser() async {
    var users = await ref.read(adminControllerProvider).getAllUsers();
    setState(() {
      userCount = users.length;
    });

    return users;
  }

  Future<List<UserModel>> searchingUser(String query) async {
    var users =
        await ref.read(adminControllerProvider).searchUsersByAllColumns(query);
    setState(() {
      userCount = users.length;
    });

    if (users.isEmpty) {
      // Tampilkan daftar pengguna kosong
      return [];
    }

    return users;
  }

  @override
  Widget build(BuildContext context) {
    final lang = Lang.of(context);
    final themeData = Theme.of(context);
    final appDataTableTheme = themeData.extension<AppDataTableTheme>()!;

    return PortalMasterLayout(
      body: ListView(
        padding: const EdgeInsets.all(kDefaultPadding),
        children: [
          Text(
            'Manage User',
            style: themeData.textTheme.headlineMedium,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: kDefaultPadding),
            child: Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const CardHeader(
                    title: 'User List',
                  ),
                  CardBody(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: kDefaultPadding * 2.0),
                          child: FormBuilder(
                            key: _formKey,
                            autovalidateMode: AutovalidateMode.disabled,
                            child: SizedBox(
                              width: double.infinity,
                              child: Wrap(
                                direction: Axis.horizontal,
                                spacing: kDefaultPadding,
                                runSpacing: kDefaultPadding,
                                alignment: WrapAlignment.spaceBetween,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 300.0,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: kDefaultPadding * 1.5),
                                      child: FormBuilderTextField(
                                        name: 'search',
                                        decoration: InputDecoration(
                                          labelText: lang.search,
                                          hintText: lang.search,
                                          border: const OutlineInputBorder(),
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.always,
                                          isDense: true,
                                        ),
                                        onChanged: (value) {
                                          // Panggil metode searchingUser() saat nilai pencarian berubah
                                          setState(() {
                                            _usersFuture =
                                                searchingUser(value!);
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            right: kDefaultPadding),
                                        child: SizedBox(
                                          height: 40.0,
                                          child: ElevatedButton(
                                            style: themeData
                                                .extension<AppButtonTheme>()!
                                                .infoElevated,
                                            onPressed: () {},
                                            child: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right:
                                                              kDefaultPadding *
                                                                  0.5),
                                                  child: Icon(
                                                    Icons.search,
                                                    size: (themeData
                                                            .textTheme
                                                            .labelLarge!
                                                            .fontSize! +
                                                        4.0),
                                                  ),
                                                ),
                                                Text(lang.search),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 40.0,
                                        child: ElevatedButton(
                                          style: themeData
                                              .extension<AppButtonTheme>()!
                                              .successElevated,
                                          onPressed: () => GoRouter.of(context)
                                              .go(RouteUri.crudDetail),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right:
                                                        kDefaultPadding * 0.5),
                                                child: Icon(
                                                  Icons.add,
                                                  size: (themeData
                                                          .textTheme
                                                          .labelLarge!
                                                          .fontSize! +
                                                      4.0),
                                                ),
                                              ),
                                              Text(lang.crudNew),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: LayoutBuilder(
                            builder: (context, constraints) {
                              final double dataTableWidth =
                                  max(kScreenWidthMd, constraints.maxWidth);

                              return Scrollbar(
                                controller: _scrollController,
                                thumbVisibility: true,
                                trackVisibility: true,
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  controller: _scrollController,
                                  child: SizedBox(
                                    width: dataTableWidth,
                                    child: Theme(
                                      data: themeData.copyWith(
                                        cardTheme: appDataTableTheme.cardTheme,
                                        dataTableTheme: appDataTableTheme
                                            .dataTableThemeData,
                                      ),
                                      child: FutureBuilder<List<UserModel>>(
                                        future: _usersFuture,
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            final users = snapshot.data!;

                                            return userCount > 0
                                                ? PaginatedDataTable(
                                                    rowsPerPage: userCount <= 20
                                                        ? userCount
                                                        : 20,
                                                    showCheckboxColumn: false,
                                                    showFirstLastButtons: true,
                                                    columns: const [
                                                      DataColumn(
                                                        label: Text('No.'),
                                                        numeric: true,
                                                      ),
                                                      DataColumn(
                                                        label: Text('Name'),
                                                      ),
                                                      DataColumn(
                                                        label: Text(
                                                            'Phone Number'),
                                                        numeric: true,
                                                      ),
                                                      DataColumn(
                                                        label: Text('UID'),
                                                      ),
                                                      DataColumn(
                                                        label: Text('Actions'),
                                                      ),
                                                    ],
                                                    source:
                                                        _UsersDataTableSource(
                                                            users),
                                                  )
                                                : const Text(
                                                    "User tidak ketemu");
                                          } else if (snapshot.hasError) {
                                            return Text(
                                              'Error: ${snapshot.error}',
                                              style: TextStyle(
                                                color:
                                                    themeData.colorScheme.error,
                                              ),
                                            );
                                          } else {
                                            return const Center(
                                              child:
                                                  CircularProgressIndicator(),
                                            );
                                          }
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _UsersDataTableSource extends DataTableSource {
  final List<UserModel> _users;

  _UsersDataTableSource(this._users);

  @override
  DataRow? getRow(int index) {
    final user = _users[index];

    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text((index + 1).toString())),
        DataCell(Text(user.name)),
        DataCell(Text(user.phoneNumber)),
        DataCell(Text(user.uid)),
        DataCell(Builder(
          builder: (context) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: kDefaultPadding),
                  child: OutlinedButton(
                    onPressed: () => GoRouter.of(context)
                        .go('${RouteUri.crudDetail}?id=${user.uid}'),
                    style: Theme.of(context)
                        .extension<AppButtonTheme>()!
                        .infoOutlined,
                    child: Text(Lang.of(context).crudDetail),
                  ),
                ),
                OutlinedButton(
                  onPressed: () {},
                  style: Theme.of(context)
                      .extension<AppButtonTheme>()!
                      .errorOutlined,
                  child: Text(Lang.of(context).crudDelete),
                ),
              ],
            );
          },
        )),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _users.length;

  @override
  int get selectedRowCount => 0;
}
