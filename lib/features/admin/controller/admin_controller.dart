import 'dart:io';

import 'package:chatbro_admin/features/admin/repository/admin_repository.dart';
import 'package:chatbro_admin/features/auth/repository/auth_repository.dart';
import 'package:chatbro_admin/models/admin_model.dart';
import 'package:chatbro_admin/models/user_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final adminControllerProvider = Provider((ref) {
  final adminRepository = ref.watch(adminRepositoryProvider);
  return AdminController(adminRepository: adminRepository, ref: ref);
});

final adminDataAuthProvider = FutureProvider((ref) {
  final adminController = ref.watch(adminControllerProvider);
  return adminController.getAdminData();
});

class AdminController {
  final AdminRepository adminRepository;
  final ProviderRef ref;
  AdminController({
    required this.adminRepository,
    required this.ref,
  });

  Future<AdminModel?> getAdminData() async {
    AdminModel? admin = await adminRepository.getCurrentAdminData();
    return admin;
  }

  Future<List<AdminModel>> getAllAdmin() async {
    return adminRepository.getAllAdmins();
  }

  Future<void> updateProfile({
    required BuildContext context,
    required WidgetRef ref,
    String? username,
    Uint8List? profilePic,
  }) async {
    await adminRepository.updateProfile(
        context: context, ref: ref, username: username, profilePic: profilePic);
  }

  Future<List<UserModel>> getAllUsers() async {
    return adminRepository.getAllUsers();
  }

  Future<List<UserModel>> searchUsersByAllColumns(String query) {
    return adminRepository.searchUsersByAllColumns(query);
  }

  Future<List<AdminModel>> searchAdminByAllColumns(String query) {
    return adminRepository.searchAdminByAllColumns(query);
  }

  void addStatus(File? file, BuildContext context, String caption) {
    adminRepository.uploadStatus(
      statusImage: file,
      context: context,
      caption: caption,
    );
  }
}
