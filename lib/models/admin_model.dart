class AdminModel {
  final String username;
  final String uid;
  final String profilePic;

  final String email;

  AdminModel({
    required this.username,
    required this.uid,
    required this.profilePic,
    required this.email,
  });

  Map<String, dynamic> toMap() {
    return {
      'username': username,
      'uid': uid,
      'profilePic': profilePic,
      'email': email,
    };
  }

  factory AdminModel.fromMap(Map<String, dynamic> map) {
    return AdminModel(
      username: map['username'] ?? '',
      uid: map['uid'] ?? '',
      profilePic: map['profilePic'] ?? '',
      email: map['email'] ?? '',
    );
  }
}
