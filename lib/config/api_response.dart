class ApiResponse {
  final int statusCode;
  final dynamic data;

  ApiResponse({required this.statusCode, this.data});
}
