import 'package:chatbro_admin/common/constants/values.dart';
import 'package:chatbro_admin/constans.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserDataProvider extends ChangeNotifier {
  var _userProfileImageUrl = '';
  var _username = '';

  String get userProfileImageUrl => _userProfileImageUrl;

  String get username => _username;

  Future<void> loadAsync() async {
    final user = firebaseAuth.currentUser;
    if (user != null) {
      final userData = await FirebaseFirestore.instance
          .collection('admins')
          .doc(user.uid)
          .get();

      _username = userData.get('username') ?? '';
      _userProfileImageUrl =
          userData.get('profilePic') ?? 'https://picsum.photos/200/300';
    } else {
      _username = '';
      _userProfileImageUrl = 'https://picsum.photos/200/300';
    }

    notifyListeners();
  }

  // Future<void> setUserDataAsync({
  //   String? userProfileImageUrl,
  //   String? username,
  // }) async {
  //   final user = FirebaseAuth.instance.currentUser;
  //   if (user != null) {
  //     var shouldNotify = false;

  //     final userRef = firestore.collection('users').doc(user.uid);

  //     if (userProfileImageUrl != null &&
  //         userProfileImageUrl != _userProfileImageUrl) {
  //       _userProfileImageUrl = userProfileImageUrl;

  //       await userRef.update({'userProfileImageUrl': _userProfileImageUrl});

  //       shouldNotify = true;
  //     }

  //     if (username != null && username != _username) {
  //       _username = username;

  //       await userRef.update({'username': _username});

  //       shouldNotify = true;
  //     }

  //     if (shouldNotify) {
  //       notifyListeners();
  //     }
  //   }
  // }

  Future<void> clearUserDataAsync() async {
    final sharedPref = await SharedPreferences.getInstance();

    await sharedPref.remove(StorageKeys.username);
    await sharedPref.remove(StorageKeys.userProfileImageUrl);

    _username = '';
    _userProfileImageUrl = '';

    notifyListeners();
  }

  bool isUserLoggedIn() {
    return firebaseAuth.currentUser != null;
  }
}
