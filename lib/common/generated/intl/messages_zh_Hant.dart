// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a zh_Hant locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'zh_Hant';

  static String m0(count) =>
      "${Intl.plural(count, one: 'Tombol', other: 'Tombol')}";

  static String m1(count) =>
      "${Intl.plural(count, one: 'Warna', other: 'Warna')}";

  static String m2(count) =>
      "${Intl.plural(count, one: 'Dialog', other: 'Dialog')}";

  static String m3(value) => "Medal ieu kedah cocog jeung ${value}";

  static String m4(count) =>
      "${Intl.plural(count, one: 'Estensi', other: 'Estensi')}";

  static String m5(count) =>
      "${Intl.plural(count, one: 'Formulir', other: 'Formulir')}";

  static String m6(max) =>
      "Medal ieu kedah leuwih loba atanapi sanés jeung ${max}";

  static String m7(maxLength) =>
      "Panjang medal ieu kedah leuwih loba atanapi sanés jeung ${maxLength}";

  static String m8(min) => "Medal ieu kedah luhur atanapi sanés jeung ${min}";

  static String m9(minLength) =>
      "Panjang medal ieu kedah luhur atanapi sanés jeung ${minLength}";

  static String m10(count) =>
      "${Intl.plural(count, one: 'Pesenan Anyar', other: 'Pesenan Anyar')}";

  static String m11(count) =>
      "${Intl.plural(count, one: 'Pamaké Anyar', other: 'Pamaké Anyar')}";

  static String m12(value) => "Medal ieu teu boleh cocog jeung ${value}";

  static String m13(count) =>
      "${Intl.plural(count, one: 'Kaca', other: 'Kaca')}";

  static String m14(count) =>
      "${Intl.plural(count, one: 'Pokok pangaruhan anu teu déngéan', other: 'Pokok pangaruhan anu teu déngéan')}";

  static String m15(count) =>
      "${Intl.plural(count, one: 'Pesenan Pungkas', other: 'Pesenan Pungkas')}";

  static String m16(count) =>
      "${Intl.plural(count, one: 'Elemen UI', other: 'Elemen UI')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "account": MessageLookupByLibrary.simpleMessage("Akun"),
        "adminPortalLogin":
            MessageLookupByLibrary.simpleMessage("Login Portal Admin"),
        "appTitle":
            MessageLookupByLibrary.simpleMessage("Portal Administrasi Web"),
        "backToLogin": MessageLookupByLibrary.simpleMessage("Balik ka Login"),
        "buttonEmphasis":
            MessageLookupByLibrary.simpleMessage("Tombol Penegasan"),
        "buttons": m0,
        "cancel": MessageLookupByLibrary.simpleMessage("Batal"),
        "closeNavigationMenu":
            MessageLookupByLibrary.simpleMessage("Tutup Menu Navigasi"),
        "colorPalette": MessageLookupByLibrary.simpleMessage("Palet Warna"),
        "colorScheme": MessageLookupByLibrary.simpleMessage("Skema Warna"),
        "colors": m1,
        "confirmDeleteRecord": MessageLookupByLibrary.simpleMessage(
            "Pastikeun ngahapus catetan ieu?"),
        "confirmSubmitRecord": MessageLookupByLibrary.simpleMessage(
            "Pastikeun ngahantosan catetan ieu?"),
        "copy": MessageLookupByLibrary.simpleMessage("Salinan"),
        "creditCardErrorText": MessageLookupByLibrary.simpleMessage(
            "Medal ieu kedah ngandung nomer kartu kredit anu sah."),
        "crudBack": MessageLookupByLibrary.simpleMessage("Mulang"),
        "crudDelete": MessageLookupByLibrary.simpleMessage("Mupus"),
        "crudDetail": MessageLookupByLibrary.simpleMessage("Detil"),
        "crudNew": MessageLookupByLibrary.simpleMessage("Anyar"),
        "darkTheme": MessageLookupByLibrary.simpleMessage("Téma Laré"),
        "dashboard": MessageLookupByLibrary.simpleMessage("Dasbor"),
        "dateStringErrorText": MessageLookupByLibrary.simpleMessage(
            "Medal ieu kedah ngandung string tanggal anu sah."),
        "dialogs": m2,
        "dontHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("Teu aya akun?"),
        "email": MessageLookupByLibrary.simpleMessage("Alamat Email"),
        "emailErrorText": MessageLookupByLibrary.simpleMessage(
            "Medal ieu kedah ngandung alamat email anu sah."),
        "equalErrorText": m3,
        "error404": MessageLookupByLibrary.simpleMessage("Kasalahan 404"),
        "error404Message": MessageLookupByLibrary.simpleMessage(
            "Hapunten, kaca anu dipigawé ku anjeun henteu aya atanapi geus dihapus."),
        "error404Title": MessageLookupByLibrary.simpleMessage("Kaca Teu Aya"),
        "example": MessageLookupByLibrary.simpleMessage("Conto"),
        "extensions": m4,
        "forms": m5,
        "generalUi": MessageLookupByLibrary.simpleMessage("UI Umum"),
        "hi": MessageLookupByLibrary.simpleMessage("Hai"),
        "homePage": MessageLookupByLibrary.simpleMessage("Kaca Pangluhurna"),
        "iframeDemo":
            MessageLookupByLibrary.simpleMessage("Démonstrasi IFrame"),
        "integerErrorText": MessageLookupByLibrary.simpleMessage(
            "Medal ieu kedah ngandung angka anu sah."),
        "ipErrorText": MessageLookupByLibrary.simpleMessage(
            "Medal ieu kedah ngandung alamat IP anu sah."),
        "language": MessageLookupByLibrary.simpleMessage("Basa"),
        "lightTheme": MessageLookupByLibrary.simpleMessage("Téma Terang"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "loginNow": MessageLookupByLibrary.simpleMessage("Login Ayeuna!"),
        "logout": MessageLookupByLibrary.simpleMessage("Logout"),
        "loremIpsum": MessageLookupByLibrary.simpleMessage(
            "Larapkeun, salapanan gandrungna ogé berhenti."),
        "matchErrorText": MessageLookupByLibrary.simpleMessage(
            "Medal ieu teu cocog jeung format anu diharapkeun."),
        "maxErrorText": m6,
        "maxLengthErrorText": m7,
        "minErrorText": m8,
        "minLengthErrorText": m9,
        "myProfile": MessageLookupByLibrary.simpleMessage("Profil Saya"),
        "newOrders": m10,
        "newUsers": m11,
        "notEqualErrorText": m12,
        "numericErrorText":
            MessageLookupByLibrary.simpleMessage("Medal ieu kedah jadi angka."),
        "openInNewTab":
            MessageLookupByLibrary.simpleMessage("Buka dina Tab Énggal"),
        "pages": m13,
        "password": MessageLookupByLibrary.simpleMessage("Katasandi"),
        "passwordHelperText":
            MessageLookupByLibrary.simpleMessage("* 6 - 18 karakter"),
        "passwordNotMatch":
            MessageLookupByLibrary.simpleMessage("Katasandi teu cocog."),
        "pendingIssues": m14,
        "recentOrders": m15,
        "recordDeletedSuccessfully": MessageLookupByLibrary.simpleMessage(
            "Catetan parantos dipupuskeun pikeun hasil."),
        "recordSavedSuccessfully": MessageLookupByLibrary.simpleMessage(
            "Catetan parantos disimpen pikeun hasil."),
        "recordSubmittedSuccessfully": MessageLookupByLibrary.simpleMessage(
            "Catetan parantos dikirim pikeun hasil."),
        "register": MessageLookupByLibrary.simpleMessage("Ngamajukeun"),
        "registerANewAccount":
            MessageLookupByLibrary.simpleMessage("Ngamajukeun Akun Anyar"),
        "registerNow":
            MessageLookupByLibrary.simpleMessage("Ngamajukeun Ayeuna!"),
        "requiredErrorText":
            MessageLookupByLibrary.simpleMessage("Medal ieu teu bisa kosong."),
        "retypePassword":
            MessageLookupByLibrary.simpleMessage("Tulis Deui Katasandi"),
        "save": MessageLookupByLibrary.simpleMessage("Simpen"),
        "search": MessageLookupByLibrary.simpleMessage("Nyungsi"),
        "submit": MessageLookupByLibrary.simpleMessage("Kirimkeun"),
        "text": MessageLookupByLibrary.simpleMessage("Téks"),
        "textEmphasis":
            MessageLookupByLibrary.simpleMessage("Pangharupaan Téks"),
        "textTheme": MessageLookupByLibrary.simpleMessage("Téma Téks"),
        "todaySales": MessageLookupByLibrary.simpleMessage("Jualan Ayeuna"),
        "typography": MessageLookupByLibrary.simpleMessage("Tipografi"),
        "uiElements": m16,
        "urlErrorText": MessageLookupByLibrary.simpleMessage(
            "Medal ieu kedah ngandung alamat URL anu sah."),
        "username": MessageLookupByLibrary.simpleMessage("Ngaran Pamaké"),
        "yes": MessageLookupByLibrary.simpleMessage("Iya")
      };
}
