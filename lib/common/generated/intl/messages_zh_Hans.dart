// JANGAN DIUBAH. Ini adalah kode yang dihasilkan melalui package:intl/generate_localized.dart
// Ini adalah pustaka yang menyediakan pesan untuk bahasa Indonesia (id). Semua pesan dari program utama
// harus diduplikasi di sini dengan nama fungsi yang sama.

// Jangan khawatirkan masalah dari perlintasan umum dalam file ini.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'zh_Hans';

  static String m0(count) =>
      "${Intl.plural(count, one: 'Tombol', other: 'Tombol')}";

  static String m1(count) =>
      "${Intl.plural(count, one: 'Warna', other: 'Warna')}";

  static String m2(count) =>
      "${Intl.plural(count, one: 'Dialog', other: 'Dialog')}";

  static String m3(value) => "Bidang ini harus sama dengan ${value}";

  static String m4(count) =>
      "${Intl.plural(count, one: 'Ekstensi', other: 'Ekstensi')}";

  static String m5(count) =>
      "${Intl.plural(count, one: 'Formulir', other: 'Formulir')}";

  static String m6(max) =>
      "Bidang ini harus kurang dari atau sama dengan ${max}";

  static String m7(maxLength) =>
      "Bidang ini harus memiliki panjang kurang dari atau sama dengan ${maxLength}";

  static String m8(min) =>
      "Bidang ini harus lebih besar dari atau sama dengan ${min}";

  static String m9(minLength) =>
      "Bidang ini harus memiliki panjang lebih dari atau sama dengan ${minLength}";

  static String m10(count) =>
      "${Intl.plural(count, one: 'Pesanan Baru', other: 'Pesanan Baru')}";

  static String m11(count) =>
      "${Intl.plural(count, one: 'Pengguna Baru', other: 'Pengguna Baru')}";

  static String m12(value) => "Bidang ini tidak boleh sama dengan ${value}";

  static String m13(count) =>
      "${Intl.plural(count, one: 'Halaman', other: 'Halaman')}";

  static String m14(count) =>
      "${Intl.plural(count, one: 'Isu yang Tertunda', other: 'Isu yang Tertunda')}";

  static String m15(count) =>
      "${Intl.plural(count, one: 'Pesanan Terbaru', other: 'Pesanan Terbaru')}";

  static String m16(count) =>
      "${Intl.plural(count, one: 'Elemen Antarmuka Pengguna', other: 'Elemen Antarmuka Pengguna')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "account": MessageLookupByLibrary.simpleMessage("Akun"),
        "adminPortalLogin":
            MessageLookupByLibrary.simpleMessage("Login Portal Admin"),
        "appTitle": MessageLookupByLibrary.simpleMessage("Dashboard Web"),
        "backToLogin": MessageLookupByLibrary.simpleMessage("Kembali ke Login"),
        "buttonEmphasis":
            MessageLookupByLibrary.simpleMessage("Penekanan Tombol"),
        "buttons": m0,
        "cancel": MessageLookupByLibrary.simpleMessage("Batal"),
        "closeNavigationMenu":
            MessageLookupByLibrary.simpleMessage("Tutup Menu Navigasi"),
        "colorPalette": MessageLookupByLibrary.simpleMessage("Palet Warna"),
        "colorScheme": MessageLookupByLibrary.simpleMessage("Skema Warna"),
        "colors": m1,
        "confirmDeleteRecord": MessageLookupByLibrary.simpleMessage(
            "Anda yakin ingin menghapus catatan ini?"),
        "confirmSubmitRecord": MessageLookupByLibrary.simpleMessage(
            "Anda yakin ingin mengirim catatan ini?"),
        "copy": MessageLookupByLibrary.simpleMessage("Salin"),
        "creditCardErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini memerlukan nomor kartu kredit yang valid."),
        "crudBack": MessageLookupByLibrary.simpleMessage("Kembali"),
        "crudDelete": MessageLookupByLibrary.simpleMessage("Hapus"),
        "crudDetail": MessageLookupByLibrary.simpleMessage("Detail"),
        "crudNew": MessageLookupByLibrary.simpleMessage("Baru"),
        "darkTheme": MessageLookupByLibrary.simpleMessage("Tema Gelap"),
        "dashboard": MessageLookupByLibrary.simpleMessage("Dasbor"),
        "dateStringErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini memerlukan string tanggal yang valid."),
        "dialogs": m2,
        "dontHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("Belum punya akun?"),
        "email": MessageLookupByLibrary.simpleMessage("Alamat Email"),
        "emailErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini memerlukan alamat email yang valid."),
        "equalErrorText": m3,
        "error404": MessageLookupByLibrary.simpleMessage("Kesalahan 404"),
        "error404Message": MessageLookupByLibrary.simpleMessage(
            "Maaf, halaman yang Anda cari tidak ada atau telah dihapus."),
        "error404Title":
            MessageLookupByLibrary.simpleMessage("Halaman Tidak Ditemukan"),
        "example": MessageLookupByLibrary.simpleMessage("Contoh"),
        "extensions": m4,
        "forms": m5,
        "generalUi": MessageLookupByLibrary.simpleMessage("UI Umum"),
        "hi": MessageLookupByLibrary.simpleMessage("Halo"),
        "homePage": MessageLookupByLibrary.simpleMessage("Beranda"),
        "iframeDemo": MessageLookupByLibrary.simpleMessage("Demo IFrame"),
        "integerErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini memerlukan angka yang valid."),
        "ipErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini memerlukan alamat IP yang valid."),
        "language": MessageLookupByLibrary.simpleMessage("Bahasa"),
        "lightTheme": MessageLookupByLibrary.simpleMessage("Tema Terang"),
        "login": MessageLookupByLibrary.simpleMessage("Masuk"),
        "loginNow": MessageLookupByLibrary.simpleMessage("Masuk Sekarang"),
        "logout": MessageLookupByLibrary.simpleMessage("Keluar"),
        "loremIpsum": MessageLookupByLibrary.simpleMessage(
            "Ini adalah contoh teks yang panjang yang digunakan dalam industri percetakan dan penataan huruf."),
        "matchErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini tidak cocok dengan format yang diperlukan."),
        "maxErrorText": m6,
        "maxLengthErrorText": m7,
        "minErrorText": m8,
        "minLengthErrorText": m9,
        "myProfile": MessageLookupByLibrary.simpleMessage("Profil Saya"),
        "newOrders": m10,
        "newUsers": m11,
        "notEqualErrorText": m12,
        "numericErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini harus berupa angka."),
        "openInNewTab":
            MessageLookupByLibrary.simpleMessage("Buka di Tab Baru"),
        "pages": m13,
        "password": MessageLookupByLibrary.simpleMessage("Kata Sandi"),
        "passwordHelperText":
            MessageLookupByLibrary.simpleMessage("* 6-18 karakter"),
        "passwordNotMatch":
            MessageLookupByLibrary.simpleMessage("Kata sandi tidak cocok."),
        "pendingIssues": m14,
        "recentOrders": m15,
        "recordDeletedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Catatan berhasil dihapus."),
        "recordSavedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Catatan berhasil disimpan."),
        "recordSubmittedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Catatan berhasil dikirim."),
        "register": MessageLookupByLibrary.simpleMessage("Daftar"),
        "registerANewAccount":
            MessageLookupByLibrary.simpleMessage("Daftarkan Akun Baru"),
        "registerNow": MessageLookupByLibrary.simpleMessage("Daftar Sekarang"),
        "requiredErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini tidak boleh kosong."),
        "retypePassword":
            MessageLookupByLibrary.simpleMessage("Ketik Ulang Kata Sandi"),
        "save": MessageLookupByLibrary.simpleMessage("Simpan"),
        "search": MessageLookupByLibrary.simpleMessage("Cari"),
        "submit": MessageLookupByLibrary.simpleMessage("Kirim"),
        "text": MessageLookupByLibrary.simpleMessage("Teks"),
        "textEmphasis": MessageLookupByLibrary.simpleMessage("Penekanan Teks"),
        "textTheme": MessageLookupByLibrary.simpleMessage("Gaya Teks"),
        "todaySales":
            MessageLookupByLibrary.simpleMessage("Penjualan Hari Ini"),
        "typography": MessageLookupByLibrary.simpleMessage("Tipografi"),
        "uiElements": m16,
        "urlErrorText": MessageLookupByLibrary.simpleMessage(
            "Bidang ini memerlukan URL yang valid."),
        "username": MessageLookupByLibrary.simpleMessage("Nama Pengguna"),
        "yes": MessageLookupByLibrary.simpleMessage("Ya")
      };
}
